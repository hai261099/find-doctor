import 'package:flutter/material.dart';
import 'package:real_estate/screens/bottom_bar/bottom_bar.dart';
import 'package:real_estate/screens/explore/explore.dart';
import 'package:real_estate/screens/forgot_password/forgot_password.dart';
import 'package:real_estate/screens/introduction/introduction.dart';
import 'package:real_estate/screens/login/login.dart';
import 'package:real_estate/screens/properties/properties.dart';
import 'package:real_estate/screens/sign_up/sign_up.dart';
import 'package:real_estate/screens/agents/agents.dart';
import 'package:real_estate/screens/user_profile/user_profile.dart';

final Map<String, WidgetBuilder> routes = {
  Introduction.routeName: (context) => Introduction(),
  LoginScreen.routeName: (context) => LoginScreen(),
  ForgotPasswordScreen.routeName: (context) => ForgotPasswordScreen(),
  SignUpScreen.routeName: (context) => SignUpScreen(),
  BottomBar.routeName: (context) => BottomBar(),
  ExploreScreen.routeName: (context) => ExploreScreen(),
  PropertiesScreen.routeName: (context) => PropertiesScreen(),
  AgentsScreen.routeName: (context) => AgentsScreen(),
  UserProfile.routeName: (context) => UserProfile()
};
