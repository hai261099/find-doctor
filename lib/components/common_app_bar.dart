import 'package:flutter/material.dart';
import 'package:real_estate/screens/user_profile/user_profile.dart';
import 'package:real_estate/theme/theme.dart';

AppBar commonAppBar(title, context) {
  return AppBar(
      title: Text(title,
          style: AppTextStyles.mediumMedium(context, color: AppColors.black)),
      centerTitle: true,
      backgroundColor: AppColors.greyLight,
      elevation: 0,
      iconTheme: IconThemeData(color: AppColors.primaryA400),
      actions: [
        InkWell(
          onTap: () {
            Navigator.of(context).pushNamed(UserProfile.routeName);
          },
          child: Padding(
              padding: EdgeInsets.only(right: 15.0),
              child: CircleAvatar(
                  backgroundImage:
                      AssetImage('lib/assets/images/francesco.png'))),
        )
      ]);
}
