import 'package:flutter/material.dart';
import 'package:real_estate/theme/theme.dart';

class BuildTextFormField extends StatelessWidget {
  const BuildTextFormField({
    this.icon,
    this.labelText,
    this.type,
    this.obscure,
    Key key,
  }) : super(key: key);
  final String labelText;
  final IconData icon;
  final TextInputType type;
  final bool obscure;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      keyboardType: type,
      autovalidateMode: AutovalidateMode.always,
      obscureText: obscure,
      decoration: InputDecoration(
          labelText: labelText,
          labelStyle:
              AppTextStyles.regularMedium(context, color: AppColors.grey),
          suffixIcon: Icon(icon, color: AppColors.grey)),
    );
  }
}
