import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:real_estate/theme/theme.dart';

class DrawerScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var drawerOptions = <Widget>[];

    List<IconData> _iconOptions = [
      MaterialCommunityIcons.help_circle,
      MaterialCommunityIcons.comment_alert,
      MaterialCommunityIcons.information
    ];

    List<String> _labelOptions = ['F.A.Q', 'Feedback', 'About Us'];

    _onSelectedItem(index) {
      switch (index) {
        case 0:
          break;
        case 1:
          break;
        case 2:
          break;
        default:
          return Text('Error');
          break;
      }
      Navigator.of(context).pop();
    }

    for (int i = 0; i < _iconOptions.length; i++) {
      drawerOptions.add(ListTile(
          onTap: () => _onSelectedItem(i),
          leading: Icon(_iconOptions[i], color: AppColors.mediumPurple),
          title: Text(_labelOptions[i])));
    }

    return Drawer(
      child: ListView(
        children: [
          DrawerHeader(
              child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CircleAvatar(minRadius: 15, maxRadius: 35),
                    Text('Real Estate App',
                        style: AppTextStyles.mediumLarge(context,
                            color: AppColors.white)),
                    Text('haind1@nextpay.vn',
                        style: AppTextStyles.regularSmall(context,
                            color: AppColors.white))
                  ]),
              decoration: BoxDecoration(color: AppColors.purpleBlueviolet)),
          Padding(
              padding: EdgeInsets.only(left: 25.0),
              child: Column(children: drawerOptions))
        ],
      ),
    );
  }
}
