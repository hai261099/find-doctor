class TextUtils {
  static String text_001 = 'SKIP';
  static String text_002 = 'DONE';
  static String text_003 = 'NEXT';
  static String text_004 = 'Real Estate App';
  static String text_005 = 'Email';
  static String text_006 = 'Password';
  static String text_007 = 'LOGIN';
  static String text_008 = 'Forgot your password?';
  static String text_009 = 'SIGNUP';
  static String text_010 = 'Password Recovery';
  static String text_011 =
      'Enter the email associated with your account and we will send you an email with instrucstions to reset your password';
  static String text_012 = 'SEND EMAIL';
  static String text_013 = 'Name';
  static String text_014 = 'Username';
  static String text_015 = 'Confirm Password';
  static String text_016 = 'Popular Properties';
  static String text_017 = 'See all';
  static String text_018 = 'Popular Locations';
  static String text_019 = 'Popular Agents';
  static String text_020 = 'Home';
  static String text_021 = 'Explore';
  static String text_022 = 'Properties';
  static String text_023 = 'Agents';
  static String text_024 = 'Preferred Location';
  static String text_025 = 'Rent';
  static String text_026 = 'Sale';
  static String text_027 = 'Price Range';
  static String text_028 = 'Area Size Range';
  static String text_029 = 'Amenities';
  static String text_030 = 'Laundry';
  static String text_031 = 'Wifi';
}
