import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:real_estate/core/components/np_dialog.dart';
import 'package:real_estate/theme/theme.dart';


import 'app_const.dart';
import 'app_utils.dart';

///
/// This method is used when we need to call a method after build() function is completed.
void onWidgetBuildDone(Function function) {
  SchedulerBinding.instance.addPostFrameCallback((_) {
    function();
  });
}

double getScreenWidth(BuildContext context) {
  return MediaQuery.of(context).size.width;
}

double getScreenHeight(BuildContext context) {
  return MediaQuery.of(context).size.height;
}

bool isKeyboardVisible(BuildContext context) {
  return MediaQuery.of(context).viewInsets.bottom != 0;
}

void hideKeyboard(BuildContext context) {
  FocusScopeNode currentFocus = FocusScope.of(context);
  if (!currentFocus.hasPrimaryFocus) {
    currentFocus.unfocus();
  }
}

showNPDialog(BuildContext context,
    {String title,
    String message,
    String firstActionTitle,
    String secondActionTitle,
    Function firstAction,
    Function secondAction,
    DialogTextType type = DialogTextType.red,
    bool barrierDismissible = true}) {
  var actions = List<NPActionDialog>();
  if (firstActionTitle != null) {
    var typeBtn = ButtonType.primaryDestruction;
    switch (type) {
      case DialogTextType.primary:
        typeBtn = ButtonType.primary;
        break;
      default:
        break;
    }
    var actionButton = NPActionDialog(
      title: firstActionTitle,
      type: typeBtn,
      onPressed: firstAction,
    );
    actions.add(actionButton);
  }

  if (secondActionTitle != null) {
    var colorTitle = AppColors.black500;
    switch (type) {
      case DialogTextType.primary:
        colorTitle = AppColors.primaryA500;
        break;
      default:
        break;
    }
    var actionButton = NPActionDialog(
      title: secondActionTitle,
      type: ButtonType.secondary,
      onPressed: secondAction,
      titleColor: colorTitle,
    );
    actions.add(actionButton);
  }

  NPDialog(
          context: context,
          barrierDismissible: barrierDismissible,
          title: title,
          message: message,
          actions: actions)
      .show();
}

showDialogLocalizationKey(BuildContext context,
    {String title,
    String message,
    String firstActionTitle,
    String secondActionTitle,
    Function firstAction,
    Function secondAction,
    DialogTextType type = DialogTextType.red,
    bool barrierDismissible = true}) {
  var actions = List<NPActionDialog>();
  if (firstActionTitle != null) {
    var typeBtn = ButtonType.primaryDestruction;
    switch (type) {
      case DialogTextType.primary:
        typeBtn = ButtonType.primary;
        break;
      default:
        break;
    }
    var actionButton = NPActionDialog(
      title: AppUtils.npLocalization(context, firstActionTitle),
      type: typeBtn,
      onPressed: firstAction,
    );
    actions.add(actionButton);
  }

  if (secondActionTitle != null) {
    var colorTitle = AppColors.black500;
    switch (type) {
      case DialogTextType.primary:
        colorTitle = AppColors.primaryA500;
        break;
      default:
        break;
    }
    var actionButton = NPActionDialog(
      title: AppUtils.npLocalization(context, secondActionTitle),
      type: ButtonType.secondary,
      onPressed: secondAction,
      titleColor: colorTitle,
    );
    actions.add(actionButton);
  }

  NPDialog(
          context: context,
          barrierDismissible: barrierDismissible,
          title: AppUtils.npLocalization(context, title),
          message: AppUtils.npLocalization(context, message),
          actions: actions)
      .show();
}
