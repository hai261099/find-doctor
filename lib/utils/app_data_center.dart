import 'dart:convert';

import 'package:crypto/crypto.dart';

class AppDataCenter {
  static AppDataCenter _instance;

  AppDataCenter._create();

  static final _appIdDefault = '342423423423423423423423';
  static final _timeDefault = '12343';

  static AppDataCenter shared() {
    if (_instance == null) {
      _instance = AppDataCenter._create();
    }
    return _instance;
  }

  static String getAppId() {
    return _appIdDefault;
  }

  static String getTime() {
    var timeNow = DateTime.now().millisecondsSinceEpoch;
    // var minutes = (timeNow / 60000).round();
    // return minutes.toString();
    // return timeNow.toString();
    return _timeDefault;
  }

  static String getToken() {
    String token = '${getAppId()}${getTime()}';
    String tokenMd5 = generateMd5(token);
    return tokenMd5;
  }

  static String generateMd5(String input) {
    return md5.convert(utf8.encode(input)).toString();
  }
}

class DataKey {
  static const TOKEN_KEY = 'token';
}
