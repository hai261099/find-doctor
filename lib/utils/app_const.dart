class Const {
  static const assetsPath = 'lib/assets/images/';

  static const PACKAGE_APP_PATH = 'packages/nextpay_app_base/';
  static const FOLDER_IMAGES_NAME = 'nextpay-images';

// Fixed code
  static const currencyVND = 'VND';
  static const imageTypeDefault = 'png';
  static const numItemDefault = 1;
  static const minLengthTextField = 3;
  static const maxLengthTextField = 50;
  static const maxLengthDescTextField = 255;
  static const maxItem = 99;
}

enum ButtonType {
  primary,
  primaryDestruction,
  primaryNotice,
  disabled,
  secondary,
  secondaryDestruction
}

enum ButtonSize { normal, small, compact }

enum StepperSize { large, medium, small }

enum OrderType { dinner, takeAway }

enum PopAction {
  none,
  delete,
  insert,
  update,
  sort,
  order,
  changeStatus,
}

enum TaskAction { menu, category, menuItem }
