import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:real_estate/app/app_config.dart';
import 'package:real_estate/localization/flutter_localizations.dart';


import 'app_const.dart';

export 'app_const.dart';
export 'app_data_center.dart';

class AppUtils {
  static String assetImage(String imagePath) {
    return (AppConfig.instance.isSubApp ? Const.PACKAGE_APP_PATH : '') +
        imagePath;
  }

  static String genarateFileNameTimeByPath(String path) {
    var fileType = Const.imageTypeDefault;
    var lastPathComponent = path.split('/').last;
    if (lastPathComponent != null) {
      var lastNameComponent = lastPathComponent.split('.').last;
      if (lastNameComponent != null) {
        fileType = lastNameComponent;
      }
    }
    var timestampStr = DateTime.now().millisecondsSinceEpoch.toString();
    return timestampStr + '.' + fileType;
  }

  static String npLocalization(BuildContext context, String textKey) {
    return NPLocalizations.of(context).getStringLabel(context, textKey);
  }

  static String trim(String str) {
    var listCom = str.split(' ');
    listCom.removeWhere((element) => element == ' ' || element == '');
    return listCom.join(' ');
  }
}

class NumberValidateInputFormatter extends TextInputFormatter {
  NumberValidateInputFormatter({this.minValue, this.maxValue, this.maxDigits});
  final int maxDigits;
  final int maxValue;
  final int minValue;

  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.selection.baseOffset == 0) {
      return newValue;
    }

    if (maxDigits != null && newValue.selection.baseOffset > maxDigits) {
      return oldValue;
    }

    int value = int.parse(newValue.text);
    if (value > maxValue || value < minValue) {
      return oldValue;
    }

    String newText = value.toString();
    return newValue.copyWith(
        text: newText,
        selection: new TextSelection.collapsed(offset: newText.length));
  }
}
