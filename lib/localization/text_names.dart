class TextName {
  static const lbSearch = 'lb_search';
  static const lbTableLayout = 'lb_table_layout';
  static const lbOrder = 'lb_order';
  static const lbDish = 'lb_dish';
  static const lbUtility = 'lb_utility';
  static const lbNoData = 'lb_no_data';
  static const errorUnknown = 'error_unknown';
  static const titleMenuNew = 'title_menu_new';
  static const titleMenuModifier = 'title_menu_modifier';
  static const titleCategoryNew = 'title_category_new';
  static const titleCategoryModifier = 'title_category_modifier';
  static const titleMenuItemNew = 'title_menu_item_new';
  static const titleMenuItemModifier = 'title_menu_item_modifier';
  static const lbMenuName = 'lb_menu_name';
  static const lbCategoryName = 'lb_category_name';
  static const lbDone = 'lb_done';
  static const lbSave = 'lb_save';
  static const lbNext = 'lb_next';
  static const lbMenuContain = 'lb_menu_contain';
  static const lbSelectMenu = 'lb_select_menu';
  static const lbHintMenu = 'lb_hint_menu';
  static const lbStatusOutDateToday = 'lb_status_out_date_today';
  static const lbStatusOutDate = 'lb_status_out_date';
  static const lbSetupMenu = 'lb_setup_menu';
  static const lbSort = 'lb_sort';
  static const lbMenus = 'lb_menus';
  static const lbModifier = 'lb_modifier';
  static const lbModifier1 = 'lb_modifier_1';
  static const lbEmptyMenu = 'lb_empty_menu';
  static const lbEmptyMenuDescription = 'lb_empty_menu_description';
  static const lbEditStatusCat = 'lb_edit_status_cat';
  static const lbEditStatusCatDes = 'lb_edit_status_cat_des';
  static const lbMenu = 'lb_menu';
  static const lbEditMenu = 'lb_edit_menu';
  static const lbItem = 'lb_item';
  static const lbAddNew = 'lb_add_new';
  static const lbEditItem = 'lb_edit_item';
  static const lbMenuSetupPopupTitle1 = 'lb_menu_setup_popup_title_1';
  static const lbMenuSetupPopupSubtitle1 = 'lb_menu_setup_popup_subtitle_1';
  static const lbMenuSetupPopupTitle2 = 'lb_menu_setup_popup_title_2';
  static const lbMenuSetupPopupSubtitle2 = 'lb_menu_setup_popup_subtitle_2';
  static const lbMenuSetupPopupTitle3 = 'lb_menu_setup_popup_title_3';
  static const lbMenuSetupPopupSubtitle3 = 'lb_menu_setup_popup_subtitle_3';
  static const lbTitleNoDataModifier = 'lb_title_no_data_modifier';
  static const lbSubTitleNoDataModifier = 'lb_sub_title_no_data_modifier';
  static const btnAddModifier = 'btn_add_modifier';
  static const lbTitleEditModifier = 'lb_title_edit_modifier';
  static const lbSubTitleEditModifier = 'lb_sub_title_edit_modifier';
  static const lbTitleCountModifierFirst = 'lb_title_count_modifier_first';
  static const lbTitleCountModifierLast = 'lb_title_count_modifier_last';
  static const linkBtnEditModifier = 'link_btn_edit_modifier';
  static const linkCountBtnEditModifierFirst =
      'link_count_btn_edit_modifier_first';
  static const linkCountBtnEditModifierLast =
      'link_count_btn_edit_modifier_last';
  static const lbTitleAddModifier = 'lb_title_add_modifier';
  static const lbTitleAddModifierEdit = 'lb_title_add_modifier_edit';
  static const lbTitleNameOfModifier = 'lb_title_name_of_modifier';
  static const lbPlaceholderNameOfModifier = 'lb_placeholder_name_of_modifier';
  static const lbTitleModifierOptions = 'lb_title_modifier_options';
  static const lbTitleModifierExampleOptions =
      'lb_title_modifier_example_options';
  static const btnLinkAddOneOption = 'btn_link_add_one_option';
  static const btnFinish = 'btn_finish';
  static const btnEdit = 'btn_edit';
  static const lbTitleDetailModifierOptions =
      'lb_title_detail_modifier_options';
  static const lbTitleDetailModifierExampleOptions =
      'lb_title_detail_modifier_example_options';
  static const lbTitleOptionNoRequired = 'lb_title_option_no_required';
  static const lbTitleOptionAvariable = 'lb_title_option_avariable';
  static const lbTitleAddModifierValue = 'lb_title_add_modifier_value';
  static const lbTitleAddModifierEditValue = 'lb_title_add_modifier_edit_value';
  static const lbTitleNameOfModifierValue = 'lb_title_name_of_modifier_value';
  static const lbPlaceholderNameOfModifierValue =
      'lb_placeholder_name_of_modifier_value';
  static const lbTitlePriceOfModifierValue = 'lb_title_price_of_modifier_value';
  static const lbPlaceholderPriceOfModifierValue =
      'lb_placeholder_price_of_modifier_value';
  static const lbTitleMinValue = 'lb_title_min_value';
  static const lbTitleMaxValue = 'lb_title_max_value';
  static const lbTitleOptionAvariableOne = 'lb_title_option_avariable_one';
  static const lbTitleError = 'lb_title_error';
  static const btnTryAgain = 'btn_try_again';
  static const btnClose = 'btn_close';
  static const lbDeleteMenuTitle = 'lb_delete_menu_title';
  static const lbDeleteMenuMessage = 'lb_delete_menu_message';
  static const lbDeleteCategoryTitle = 'lb_delete_category_title';
  static const lbDeleteCategoryMessage = 'lb_delete_category_message';
  static const lbActionDelete = 'lb_action_delete';
  static const lbActionClose = 'lb_action_close';
  static const lbActionSave = 'lb_action_save';
  static const lbActionSaveContinue = 'lb_action_save_continue';
  static const lbActionCancel = 'lb_action_cancel';
  static const lbActionPhotos = 'lb_action_photos';
  static const lbActionCamera = 'lb_action_camera';
  static const lbActionNext = 'lb_action_next';
  static const lbMenuItemSetupInfo = 'lb_menu_item_setup_info';
  static const lbMenuItemSetupCategory = 'lb_menu_item_setup_category';
  static const lbMenuItemSetupOption = 'lb_menu_item_setup_option';
  static const lbMenuItemSetupUploadImage = 'lb_menu_item_setup_upload_image';
  static const lbMenuItemSetupUploadImageDesc =
      'lb_menu_item_setup_upload_image_desc';
  static const lbMenuItemSetupName = 'lb_menu_item_setup_name';
  static const lbMenuItemSetupCode = 'lb_menu_item_setup_code';
  static const lbMenuItemSetupPrice = 'lb_menu_item_setup_price';
  static const lbMenuItemSetupUnit = 'lb_menu_item_setup_unit';
  static const lbMenuItemSetupDesc = 'lb_menu_item_setup_desc';
  static const lbMenuItemSetupDescHint = 'lb_menu_item_setup_desc_hint';
  static const lbMenuItemSetupDisplay = 'lb_menu_item_setup_display';
  static const lbMenuItemSetupDisplayDesc = 'lb_menu_item_setup_display_desc';
  static const loading = 'loading';
  static const lbCartOrdered = 'lb_cart_ordered';
  static const lbMenuList = 'lb_menu_list';
  static const lbDeleteMenuModifierTitle = 'lb_delete_menu_modifier_title';
  static const lbDeleteMenuModifierMessage = 'lb_delete_menu_modifier_message';
  static const lbDeleteMenuModifierValueTitle =
      'lb_delete_menu_modifier_value_title';
  static const lbDeleteMenuModifierValueMessage =
      'lb_delete_menu_modifier_value_message';
  static const lbChangedMenuModifierTitle = 'lb_changed_menu_modifier_title';
  static const lbChangedMenuModifierMessage =
      'lb_changed_menu_modifier_message';
  static const lbChangedMenuModifierValueTitle =
      'lb_changed_menu_modifier_value_title';
  static const lbChangedMenuModifierValueMessage =
      'lb_changed_menu_modifier_value_message';
  static const lbActionCancelChanged = 'lb_action_cancel_changed';
  static const messOptionAvariableOne = 'mess_option_avariable_one';
  static const messOptionNoRequiredFirst = 'mess_option_no_required_first';
  static const messOptionNoRequiredLast = 'mess_option_no_required_last';
  static const messOptionRequiredAvariableFirst =
      'mess_option_required_avariable_first';
  static const messOptionRequiredAvariableLast =
      'mess_option_required_avariable_last';
  static const lbReorderMenu = 'lb_reorder_menu';
  static const lbReorderCategory = 'lb_reorder_category';
  static const lbReorderItem = 'lb_reorder_item';
  static const lbStatusChange = 'lb_status_change';
  static const lbStatusChangeShort = 'lb_status_change_short';
  static const lbDialogTitleNormal = 'lb_dialog_title_normal';
  static const lbDialogTitleError = 'lb_dialog_title_error';
  static const lbDialogUpdateMenuTitle = 'lb_dialog_update_menu_title';
  static const lbDialogUpdateMenuSuccessMesg =
      'lb_dialog_update_menu_success_mesg';
  static const lbDialogCreateMenuTitle = 'lb_dialog_create_menu_title';
  static const lbDialogDeleteMenuTitle = 'lb_dialog_delete_menu_title';
  static const lbDialogDeleteMenuSuccessMesg =
      'lb_dialog_delete_menu_success_mesg';
  static const lbDialogDeleteMenuConfirmMesg =
      'lb_dialog_delete_menu_confirm_mesg';
  static const lbDialogUpdateCategoryTitle = 'lb_dialog_update_category_title';
  static const lbDialogUpdateCategorySuccessMesg =
      'lb_dialog_update_category_success_mesg';
  static const lbDialogCreateCategoryTitle = 'lb_dialog_create_category_title';
  static const lbDialogDeleteCategoryTitle = 'lb_dialog_delete_category_title';
  static const lbDialogDeleteCategorySuccessMesg =
      'lb_dialog_delete_category_success_mesg';
  static const lbDialogDeleteCategoryConfirmMesg =
      'lb_dialog_delete_category_confirm_mesg';
  static const lbDialogUpdateMenuItemTitle = 'lb_dialog_update_menu_item_title';
  static const lbDialogUpdateMenuItemSuccessMesg =
      'lb_dialog_update_menu_item_success_mesg';
  static const lbDialogCreateMenuItemTitle = 'lb_dialog_create_menu_item_title';
  static const lbDialogDeleteMenuItemTitle = 'lb_dialog_delete_menu_item_title';
  static const lbDialogDeleteMenuItemSuccessMesg =
      'lb_dialog_delete_menu_item_success_mesg';
  static const lbDialogDeleteMenuItemConfirmMesg =
      'lb_dialog_delete_menu_item_confirm_mesg';
  static const lbDialogExitMenuModifierUpdateChangedTitle =
      'lb_dialog_exit_menu_modifier_update_changed_title';
  static const lbDialogExitMenuModifierUpdateChangedMesg =
      'lb_dialog_exit_menu_modifier_update_changed_mesg';
  static const lbDialogExitCategoryModifierUpdateChangedTitle =
      'lb_dialog_exit_category_modifier_update_changed_title';
  static const lbDialogExitCategoryModifierUpdateChangedMesg =
      'lb_dialog_exit_category_modifier_update_changed_mesg';
  static const lbDialogExitMenuItemModifierUpdateChangedTitle =
      'lb_dialog_exit_menu_item_modifier_update_changed_title';
  static const lbDialogExitMenuItemModifierUpdateChangedMesg =
      'lb_dialog_exit_menu_item_modifier_update_changed_mesg';
  static const lbDialogExistMenuTitle = 'lb_dialog_exist_menu_title';
  static const lbDialogExistMenuMesg = 'lb_dialog_exist_menu_mesg';
  static const lbDialogExistCategoryTitle = 'lb_dialog_exist_category_title';
  static const lbDialogExistCategoryMesg = 'lb_dialog_exist_category_mesg';
  static const lbDialogAddImageTitle = 'lb_dialog_add_image_title';
  static const lbDialogRemoveItemOrderTitle =
      'lb_dialog_remove_item_order_title';
  static const lbDialogRemoveItemOrderMesgStart =
      'lb_dialog_remove_item_order_mesg_start';
  static const lbDialogRemoveItemOrderMesgEnd =
      'lb_dialog_remove_item_order_mesg_end';
  static const lbUnlink = 'lb_unlink';
  static const lbLink = 'lb_link';
  static const lbTitleGuideLinkToModifier = 'lb_title_guide_link_to_modifier';
  static const lbSubTitleGuideLinkToModifier =
      'lb_sub_title_guide_link_to_modifier';
  static const lbTitleOptionGroupOf = 'lb_title_option_group_of';
  static const lbTitleItemOptionGroupFirst = 'lb_title_item_option_group_first';
  static const lbTitleItemOptionGroupLast = 'lb_title_item_option_group_last';
  static const lbOptionSelect = 'lb_option_select';
  static const lbChangedMenuReorderTitle = 'lb_changed_menu_reorder_title';
  static const lbChangedMenuReorderMessage = 'lb_changed_menu_reorder_message';
  static const lbGeneral = 'lb_general';
  static const lbSellingHistory = 'lb_selling_history';
  static const lbAnalyticsReport = 'lb_analytics_report';
  static const lbSetupStore = 'lb_setup_store';
  static const lbSetupZoneTable = 'lb_setup_zone_table';
  static const btnAddNewOption = 'btn_add_new_option';
  static const errorNameOfModifierValue = 'error_name_of_modifier_value';
  static const errorNameOfModifier = 'error_name_of_modifier';
  static const msgInsertModifierValue = 'msg_insert_modifier_value';
  static const msgUpdateModifierValue = 'msg_update_modifier_value';
  static const msgDeleteModifierValue = 'msg_delete_modifier_value';
  static const msgInsertModifier = 'msg_insert_modifier';
  static const msgUpdateModifier = 'msg_update_modifier';
  static const msgDeleteModifier = 'msg_delete_modifier';
  static const lbTitleModifierOptionGroupFirst =
      'lb_title_modifier_option_group_first';
  static const lbTitleModifierOptionGroupLast =
      'lb_title_modifier_option_group_last';
  static const errorLinkItemToModifier = 'error_link_item_to_modifier';
  static const errorUnlinkItemToModifier = 'error_unlink_item_to_modifier';
  static const errorLinkModifierToItem = 'error_link_modifier_to_item';
  static const errorUnlinkModifierToItem = 'error_unlink_modifier_to_item';
  static const s01 = 's01';
  static const s02 = 's02';
  static const s03 = 's03';
  static const cfgUnitKilogram = 'CFG_UNIT_KILOGRAM';
  static const cfgUnitKettle = 'CFG_UNIT_KETTLE';
  static const cfgUnitPack = 'CFG_UNIT_PACK';
  static const cfgUnitBowl = 'CFG_UNIT_BOWL';
  static const cfgUnitPiece = 'CFG_UNIT_PIECE';
  static const cfgUnitBottle = 'CFG_UNIT_BOTTLE';
  static const cfgUnitCup = 'CFG_UNIT_CUP';
  static const cfgUnitA = 'CFG_UNIT_A';
  static const cfgUnitBar = 'CFG_UNIT_BAR';
  static const cfgUnitDish = 'CFG_UNIT_DISH';
  static const cfgUnitGram = 'CFG_UNIT_GRAM';
  static const cfgUnitCarton = 'CFG_UNIT_CARTON';
  static const cfgUnitCan = 'CFG_UNIT_CAN';
  static const cfgUnitGlass = 'CFG_UNIT_GLASS';
  static const cfgUnitPot = 'CFG_UNIT_POT';
  static const cfgUnitPart = 'CFG_UNIT_PART';
  static const cfgUnitFruit = 'CFG_UNIT_FRUIT';
  static const cfgUnitScoop = 'CFG_UNIT_SCOOP';
  static const cfgUnitStubborn = 'CFG_UNIT_STUBBORN';
  static const lbStatusSoldOut = 'lb_status_sold_out';
  static const lbStatusAvaiable = 'lb_status_avaiable';
  static const lbHelperCategoryNameBlank = 'lb_helper_category_name_blank';
  static const lbHelperCategoryNameInvalid = 'lb_helper_category_name_invalid';
  static const lbHelperMenuItemNameBlank = 'lb_helper_menu_item_name_blank';
  static const lbHelperMenuItemNameInvalid = 'lb_helper_menu_item_name_invalid';
  static const lbToastDeleteMenuSuccess = 'lb_toast_delete_menu_success';
  static const lbToastNewMenuSuccess = 'lb_toast_new_menu_success';
  static const lbToastEditMenuSuccess = 'lb_toast_edit_menu_success';
  static const lbToastNewCategorySuccess = 'lb_toast_new_category_success';
  static const lbToastEditCategorySuccess = 'lb_toast_edit_category_success';
  static const lbToastDeleteCategorySuccess =
      'lb_toast_delete_category_success';
  static const lbToastNewMenuItemSuccess = 'lb_toast_new_menu_item_success';
  static const lbToastEditMenuItemSuccess = 'lb_toast_edit_menu_item_success';
  static const lbToastDeleteMenuItemSuccess =
      'lb_toast_delete_menu_item_success';
  static const lbDialogCreateCategorySuccessMesg =
      'lb_dialog_create_category_success_mesg';
  static const lbDialogCreateMenuItemSuccessMesg =
      'lb_dialog_create_menu_item_success_mesg';
  static const lbDialogCreateMenuSuccessMesg =
      'lb_dialog_create_menu_success_mesg';
  static const lbHelperMenuNameBlank = 'lb_helper_menu_name_blank';
  static const lbHelperMenuNameInvalid = 'lb_helper_menu_name_invalid';
  static const lbToastUpdateMenuItemStatusSuccess =
      'lb_toast_update_menu_item_status_success';
  static const lbToastUpdateModifierStatusSuccess =
      'lb_toast_update_modifier_status_success';
  static const lbToastUpdateOrdinalSuccess = 'lb_toast_update_ordinal_success';
  static const lbMenuOptionNotRequired = 'lb_menu_option_not_required';
  static const lbMenuOptionMaxCheck = 'lb_menu_option_max_check';
  static const lbMenuOptionRequired = 'lb_menu_option_required';
  static const lbMenuOptionCustomerNote = 'lb_menu_option_customer_note';
  static const lbMenuOptionCustomerNoteHint =
      'lb_menu_option_customer_note_hint';
  static const lbMenuOptionUpdateItemOrder = 'lb_menu_option_update_item_order';
  static const lbMenuOptionAddItemOrder = 'lb_menu_option_add_item_order';
  static const lbMenuOrderDinner = 'lb_menu_order_dinner';
  static const lbMenuOrderTakeAway = 'lb_menu_order_take_away';
  static const lbMenuOrderCustomer = 'lb_menu_order_customer';
  static const lbMenuOrderTable = 'lb_menu_order_table';
  static const lbMenuOrderListItem = 'lb_menu_order_list_item';
  static const lbMenuOrderItem = 'lb_menu_order_item';
  static const lbMenuOrderCalculateTemp = 'lb_menu_order_calculate_temp';
  static const lbMenuOrderPay = 'lb_menu_order_pay';
  static const lbMenuOrderOrder = 'lb_menu_order_order';
  static const lbMenuOrderItemEdit = 'lb_menu_order_item_edit';
  static const lbMenuOrderTitle = 'lb_menu_order_title';
  static const lbMenuOrderCustomerNoteTitle =
      'lb_menu_order_customer_note_title';
  static const lbMenuOrderCustomerPhone = 'lb_menu_order_customer_phone';
  static const lbMenuOrderCustomerPhoneHint =
      'lb_menu_order_customer_phone_hint';
  static const lbMenuOrderCustomerDone = 'lb_menu_order_customer_done';
  static const lbEmptyCategory = 'lb_empty_category';
  static const lbEmptyCategoryItem = 'lb_empty_category_item';

  static const lbErrorCamera = 'lb_error_camera';
  static const lbRetry = 'lb_retry';
}
