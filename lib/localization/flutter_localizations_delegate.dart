import 'dart:async';

import 'package:flutter/material.dart';

import 'flutter_localizations.dart';

class NPLocalizationsDelegate extends LocalizationsDelegate<NPLocalizations> {
  static List<Locale> get supportedLocales =>
      [const Locale('en', 'US'), const Locale('vi', 'VN')];

  final bool isTest;

  const NPLocalizationsDelegate({
    this.isTest = false,
  });

  @override
  bool isSupported(Locale locale) {
    if (supportedLocales.any((l) => l.languageCode == locale.languageCode)) {
      return true;
    }
    print(
        'Missing support for requested locale ${locale.countryCode}|${locale.languageCode}.');
    return false;
  }

  @override
  Future<NPLocalizations> load(Locale locale) async {
    NPLocalizations localizations =
        new NPLocalizations(locale, isTest: this.isTest);

    if (this.isTest) {
      await localizations.loadTest(locale);
    } else {
      await localizations.load();
    }

    print('Load ${locale.languageCode}');

    return localizations;
  }

  @override
  bool shouldReload(LocalizationsDelegate<NPLocalizations> old) {
    return false;
  }
}
