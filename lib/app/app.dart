import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:real_estate/localization/flutter_localizations_delegate.dart';
import 'package:real_estate/screens/login/login.dart';
import 'package:real_estate/store/app_store.dart';
import 'package:real_estate/theme/theme.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

class Application extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ApplicationState();
  }
}

class _ApplicationState extends State<Application> {
  AppStore _store;
  Key _key = UniqueKey();

  @override
  void initState() {
    // AppDataCenter.shared().initDataCenter();

    if (_store == null) {
      _store = AppStore();
    }
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider<AppStore>(create: (_) => _store),
      ],
      child: MaterialApp(
        title: 'NextPay App',
        localizationsDelegates: [
          GlobalCupertinoLocalizations.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          NPLocalizationsDelegate(),
        ],
        initialRoute: 'app/splash',
        routes: {
          'app/splash': (context) => LoginScreen()
        },
        debugShowCheckedModeBanner: false,
        localeResolutionCallback:
            (Locale locale, Iterable<Locale> supportedLocales) {
          if (locale == null) {
            debugPrint('*language locale is null!!!');
            return supportedLocales.first;
          }
          return supportedLocales.first;
        },
        theme: AppThemes.defaultTheme,
        // onGenerateRoute: AppRouter.router.generator,
      ),
    );
  }
}
