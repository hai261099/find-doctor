import 'package:flutter/material.dart';

class AppColors {
  static const Map<int, Color> colorMaterialMap = {
    50: Color.fromRGBO(136, 14, 79, .1),
    100: Color.fromRGBO(136, 14, 79, .2),
    200: Color.fromRGBO(136, 14, 79, .3),
    300: Color.fromRGBO(136, 14, 79, .4),
    400: Color.fromRGBO(136, 14, 79, .5),
    500: Color.fromRGBO(136, 14, 79, .6),
    600: Color.fromRGBO(136, 14, 79, .7),
    700: Color.fromRGBO(136, 14, 79, .8),
    800: Color.fromRGBO(136, 14, 79, .9),
    900: Color.fromRGBO(136, 14, 79, 1),
  };

  // Brand Color
  static const brandA500 = Color(0xFF248EB);
  static const brandB500 = Color(0xFF00B855);

  // Primary Color
  static const primaryA500 = Color(0xFF2F6BFF);
  static const primaryA400 = Color(0xFF5989FF);
  static const primaryA300 = Color(0xFFC1D3FF);
  static const primaryA200 = Color(0xFFEAF0FF);
  static const primaryA100 = Color(0xFFF5F8FF);

  // Ink Color
  static const ink500 = Color(0xFF22313F);
  static const ink400 = Color(0xFF90989F);
  static const ink300 = Color(0xFFBDC1C5);
  static const ink200 = Color(0xFFE9EAEC);
  static const ink100 = Color(0xFFF4F5F5);

  // Black Color
  static const black500 = Color(0xFF000000);
  static const black400 = Color.fromRGBO(0, 0, 0, 0.7);
  static const black300 = Color.fromRGBO(0, 0, 0, 0.4);
  static const black200 = Color.fromRGBO(0, 0, 0, 0.3);
  static const black100 = Color.fromRGBO(0, 0, 0, 0.1);

  // White Color
  static const white500 = Color(0xFFFFFFFF);
  static const white400 = Color.fromRGBO(255, 255, 255, 0.7);
  static const white300 = Color.fromRGBO(255, 255, 255, 0.4);
  static const white200 = Color.fromRGBO(255, 255, 255, 0.3);
  static const white100 = Color.fromRGBO(255, 255, 255, 0.1);

  // Blue Color
  static const blue500 = Color(0xFF2F6BFF);
  static const blue400 = Color(0xFF5989FF);
  static const blue300 = Color(0xFFC1D3FF);
  static const blue200 = Color(0xFFEAF0FF);
  static const blue100 = Color.fromRGBO(47, 107, 255, 0.05);

  // Green Color
  static const green500 = Color(0xFF00BC3C);
  static const green400 = Color(0xFF33C963);
  static const green300 = Color(0xFFB3EBC5);
  static const green200 = Color(0xFFE6F8EC);
  static const green100 = Color(0xFFF2FCF5);

  // Red Color
  static const red500 = Color(0xFFDC2323);
  static const red400 = Color(0xFFEB3B5B);
  static const red300 = Color(0xFFF8B6C2);
  static const red200 = Color(0xFFFCE7EB);
  static const red100 = Color(0xFFFEF3F5);

  // Orange Color
  static const orange500 = Color(0xFFFF821E);
  static const orange400 = Color(0xFFFF9B4B);
  static const orange300 = Color(0xFFFFDABC);
  static const orange200 = Color(0xFFFFF3E9);
  static const orange100 = Color(0xFFFFF9F4);

  // Purple Color
  static const purpleIndigo = Color(0xFF4B0082);
  static const purple = Color(0xFF800080);
  static const purpleDarkmagenta = Color(0xFF8B008B);
  static const purpleDarkorchid = Color(0xFF9932CC);
  static const purpleDarkviolet = Color(0xFF9400D3);
  static const purpleBlueviolet = Color(0xFF8A2BE2);
  static const mediumPurple = Color(0xFF9370DB);
  static const purpleMediumorchid = Color(0xFFBA55D3);
  static const purpleMagenta = Color(0xFFFF00FF);
  static const purpleFuchsia = Color(0xFFFF00FF);
  static const purpleOrchid = Color(0xFFDA70D6);
  static const purpleViolet = Color(0xFFEE82EE);
  static const purplePlum = Color(0xFFDDA0DD);
  static const purpleThistle = Color(0xFFD8BFD8);
  static const purpleLavender = Color(0xFFE6E6FA);

  // Support Color
  static const backgroundLight = Color(0xFFFAFDFF);
  static const backgroundDark = Color(0xFFEBF2FF);
  static const placeholder = Color(0xFFE8EDF3);

  static const colorAccent = Color(0xFF07AB51);
  static const green = MaterialColor(0xFF1AAF53, colorMaterialMap);
  static const blue = MaterialColor(0xFF157EFB, colorMaterialMap);
  static const greyDark = Color(0xFF303C52);
  static const grey = Color(0xFF5A6385);
  static const greyLight = Color(0xFFF2F2F2);
  static const orange = Color(0xFFFF6F1A);
  static const red = Color(0xFFEB0400);
  static const white = Color(0xFFFFFFFF);
  static const black = Color(0xFF000000);

  // Divider
  static const divider = Color(0xFFE9EAEC);
}
