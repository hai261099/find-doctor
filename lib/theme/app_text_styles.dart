import 'package:flutter/material.dart';

import 'theme.dart';

class AppTextStyles {
  // Text style with font Normal
  static TextStyle regular(BuildContext context,
      {@required double size,
      Color color = AppColors.ink500,
      double lineHeight = AppDimens.lineHeightXSmall}) {
    var height = 1.0;
    if (lineHeight > size) {
      height = lineHeight / size;
    }
    return Theme.of(context).textTheme.subtitle1.copyWith(
        fontSize: size,
        fontWeight: FontWeight.normal,
        color: color,
        height: height);
  }

  // text size 12
  static TextStyle regularXSmall(BuildContext context,
      {Color color = AppColors.ink500,
      double lineHeight = AppDimens.lineHeightXSmall}) {
    return AppTextStyles.regular(context,
        size: AppDimens.textSizeXSmall, color: color, lineHeight: lineHeight);
  }

  // text size 14
  static TextStyle regularSmall(BuildContext context,
      {Color color = AppColors.ink500,
      double lineHeight = AppDimens.lineHeightSmall}) {
    return AppTextStyles.regular(context,
        size: AppDimens.textSizeSmall,
        color: color,
        lineHeight: AppDimens.lineHeightSmall);
  }

  // text size 16
  static TextStyle regularMedium(BuildContext context,
      {Color color = AppColors.ink500,
      double lineHeight = AppDimens.lineHeightMedium}) {
    return AppTextStyles.regular(context,
        size: AppDimens.textSizeMedium, color: color, lineHeight: lineHeight);
  }

  // text size 18
  static TextStyle regularXLarge(BuildContext context,
      {Color color = AppColors.ink500,
      double lineHeight = AppDimens.lineHeightMedium}) {
    return AppTextStyles.regular(context,
        size: AppDimens.textSizeXLarge, color: color, lineHeight: lineHeight);
  }

  // text size 24
  static TextStyle regularLarge(BuildContext context,
      {Color color = AppColors.ink500,
      double lineHeight = AppDimens.lineHeightMedium}) {
    return AppTextStyles.regular(context,
        size: AppDimens.spaceLarge, color: color, lineHeight: lineHeight);
  }

  // Text style with font Medium
  static TextStyle medium(BuildContext context,
      {@required double size,
      Color color = AppColors.ink500,
      double lineHeight = AppDimens.lineHeightXSmall}) {
    var height = 1.0;
    if (lineHeight > size) {
      height = lineHeight / size;
    }
    return Theme.of(context).textTheme.subtitle1.copyWith(
        fontSize: size,
        fontWeight: FontWeight.w500,
        color: color,
        height: height);
  }

  // text size 12
  static TextStyle mediumXSmall(BuildContext context,
      {Color color = AppColors.ink500,
      double lineHeight = AppDimens.lineHeightXSmall}) {
    return AppTextStyles.medium(context,
        size: AppDimens.textSizeXSmall, color: color, lineHeight: lineHeight);
  }

  // text size 14
  static TextStyle mediumSmall(BuildContext context,
      {Color color = AppColors.ink500,
      double lineHeight = AppDimens.lineHeightSmall}) {
    return AppTextStyles.medium(context,
        size: AppDimens.textSizeSmall, color: color, lineHeight: lineHeight);
  }

  // text size 16
  static TextStyle mediumMedium(BuildContext context,
      {Color color = AppColors.ink500,
      double lineHeight = AppDimens.lineHeightMedium}) {
    return AppTextStyles.medium(context,
        size: AppDimens.textSizeMedium, color: color, lineHeight: lineHeight);
  }

  // text size 18
  static TextStyle mediumXLarge(BuildContext context,
      {Color color = AppColors.ink500,
      double lineHeight = AppDimens.lineHeightMedium}) {
    return AppTextStyles.medium(context,
        size: AppDimens.textSizeXLarge, color: color, lineHeight: lineHeight);
  }

  // text size 24
  static TextStyle mediumLarge(BuildContext context,
      {Color color = AppColors.ink500,
      double lineHeight = AppDimens.lineHeightMedium}) {
    return AppTextStyles.medium(context,
        size: AppDimens.textSizeLarge, color: color, lineHeight: lineHeight);
  }

  static TextStyle bold(BuildContext context,
      {@required double size,
      Color color = AppColors.ink500,
      double lineHeight = AppDimens.lineHeightXSmall}) {
    var height = 1.0;
    if (lineHeight > size) {
      height = lineHeight / size;
    }
    return Theme.of(context).textTheme.subtitle1.copyWith(
        fontSize: size,
        fontWeight: FontWeight.bold,
        color: color,
        height: height);
  }
}
