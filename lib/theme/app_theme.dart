import 'package:flutter/material.dart';

import 'theme.dart';

class AppThemes {
  static final TextTheme defaultTextTheme = TextTheme(
    headline1: TextStyle(
        color: AppColors.black, fontSize: 96, fontWeight: FontWeight.normal),
    headline2: TextStyle(
        color: AppColors.black, fontSize: 60, fontWeight: FontWeight.normal),
    headline3: TextStyle(
        color: AppColors.black, fontSize: 48, fontWeight: FontWeight.normal),
    headline4: TextStyle(
        color: AppColors.black, fontSize: 34, fontWeight: FontWeight.normal),
    headline5: TextStyle(
        color: AppColors.black,
        fontSize: AppDimens.textSizeLarge,
        fontWeight: FontWeight.normal),
    headline6: TextStyle(
        color: AppColors.black,
        fontSize: AppDimens.textSizeXLarge,
        fontWeight: FontWeight.normal),
    subtitle1: TextStyle(
        color: AppColors.black,
        fontSize: AppDimens.textSizeMedium,
        fontWeight: FontWeight.normal),
    subtitle2: TextStyle(
        color: AppColors.black,
        fontSize: AppDimens.textSizeMedium,
        fontWeight: FontWeight.w500),
    bodyText1: TextStyle(
        color: AppColors.black,
        fontSize: AppDimens.textSizeSmall,
        fontWeight: FontWeight.normal),
    bodyText2: TextStyle(
        color: AppColors.black,
        fontSize: AppDimens.textSizeSmall,
        fontWeight: FontWeight.w500),
    button: TextStyle(
        color: AppColors.black,
        fontSize: AppDimens.textSizeXSmall,
        fontWeight: FontWeight.bold),
    caption: TextStyle(
        color: AppColors.black,
        fontSize: AppDimens.textSizeXSmall,
        fontWeight: FontWeight.w500),
    overline: TextStyle(
        color: AppColors.black,
        fontSize: AppDimens.textSizeXSmall,
        fontWeight: FontWeight.normal),
  );

  static final ThemeData defaultTheme = ThemeData(
    fontFamily: AppFonts.roboto,
    primarySwatch: AppColors.blue,
    scaffoldBackgroundColor: AppColors.backgroundDark,
    backgroundColor: Colors.white,
    indicatorColor: AppColors.ink300,
    splashColor: Colors.transparent,
    highlightColor: Colors.transparent,
    accentColor: AppColors.blue500,
    hoverColor: AppColors.placeholder,
    errorColor: AppColors.red,
    hintColor: AppColors.placeholder,
    brightness: Brightness.light,
    iconTheme: IconThemeData(
      color: AppColors.ink400,
    ),
    floatingActionButtonTheme: FloatingActionButtonThemeData(
      backgroundColor: AppColors.brandB500,
      foregroundColor: AppColors.white,
    ),
    appBarTheme: AppBarTheme(
        brightness: Brightness.light,
        color: AppColors.blue500,
        centerTitle: false,
        actionsIconTheme: IconThemeData(color: AppColors.white),
        iconTheme: IconThemeData(color: AppColors.white),
        textTheme: defaultTextTheme.copyWith(
          headline6: TextStyle(
              color: AppColors.white,
              fontSize: AppDimens.textSizeXLarge,
              fontWeight: FontWeight.normal),
        )),
    tabBarTheme: TabBarTheme(
        labelColor: AppColors.green,
        unselectedLabelColor: AppColors.ink400,
        labelStyle: TextStyle(fontWeight: FontWeight.bold)),
    cardTheme: CardTheme(
        elevation: 2,
        color: Colors.white,
        clipBehavior: Clip.hardEdge,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(8)))),
    dialogTheme: DialogTheme(
        elevation: 3,
        backgroundColor: Colors.white,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(8)))),
    textTheme: defaultTextTheme,
    inputDecorationTheme: InputDecorationTheme(
      border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
    ),
    unselectedWidgetColor: AppColors.ink300,
    buttonTheme: ButtonThemeData(
      textTheme: ButtonTextTheme.primary,
      buttonColor: AppColors.primaryA500,
      height: 48,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8)),
          side: BorderSide(style: BorderStyle.none)),
    ),
  );
}
