class Images {
  static const String _internalImagePath = 'lib/assets/images/';

  static const icDefaultItemThumb = _internalImagePath + 'ic_default_item_thumb.png';

  static const icMenu2 = _internalImagePath + 'ic_menu_2.png';
  static const icMenu6 = _internalImagePath + 'ic_menu_6.png';
  static const icMenuWithCount = _internalImagePath + 'ic_menu_with_count.png';
  static const icTableLayout = _internalImagePath + 'ic_table_layout.png';
  static const icOrder = _internalImagePath + 'ic_order.png';
  static const icMenu = _internalImagePath + 'ic_menu.png';
  static const icUtility = _internalImagePath + 'ic_utility.png';

  static const icDinner = _internalImagePath + 'ic_dinner.png';
  static const icTakeAway = _internalImagePath + 'ic_takeAway.png';
  static const imgDefault = _internalImagePath + 'image_default.png';
  static const icTrashWhite = _internalImagePath + 'ic_trash_white.png';

  static const icEmptyMenu = _internalImagePath + 'ic_empty_menu.png';

  static const icAddCat = _internalImagePath + 'ic_add_cat.png';
  static const icAddMenu = _internalImagePath + 'ic_add_menu.png';
  static const icAddMenuItem = _internalImagePath + 'ic_add_menu_item.png';
  // rau trang modifier setup
  static const icSort = _internalImagePath + 'ic_sort.png';
  static const imgNoModifier = _internalImagePath + 'image_no_modifer.png';
  static const icChangeStatus = _internalImagePath + 'ic_change_status.png';
  static const icError = _internalImagePath + 'ic_error.png';
  static const icCheckCircleBlue = _internalImagePath + 'ic_check_circle_blue.png';
  static const icUploadImage = _internalImagePath + 'ic_upload_image.png';
  static const imgSplash = _internalImagePath + 'img_splash.png';
  static const itemImageDefault = _internalImagePath + 'item_image_default.png';
}
